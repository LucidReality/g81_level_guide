# g81_level_guide

I wrote this tool to facilitate the process of leveling my bed after changing out my fixed spacers for nyloc nuts per this article:

https://github.com/PrusaOwners/prusaowners/wiki/Bed_Leveling_without_Wave_Springs

I do not want to modify my firmware, so I have to do math. As I'm terrible at math but good at code, I decided to let my laptop do the work for me.

Please note that this is a work in progress and other than verifying that it performs the desired function **for me**, I have not gone through a full leveling process yet.

I offer no claims of warranty nor perfection. I strongly suggest you review the code to make sure it does what I claim and I'm not suckering you into damaging your printer.

I'm all ears if you have issues or have suggestions.

## What it does

The tool will connect to your printer over it's serial interface. After initialization (see `Modes` below) it performs the following steps:

1) Issues the `G80` command to generate the bed metrics.
2) Issues a `G1` command to raise the extruder and "eject" the bed (Z100/Y200) to facilitate adjusting the screws.
3) Fetches the bed metrics using `G81`.
4) Calculates the worst adjustment point and tells you which one to adjust.
5) Asks you if you want to repeat the process.

It will continue until you tell it to stop, at which point it will give you the last bed leveling metrics that it read for the 8 points we care about.

This allows you to iterate through your adjustments without having to modify your firmware or copy/paste values anywhere.

## Modes

This is pretty basic code, so it needs your help. There are two cases for talking to the printer.

The first is that you have not connected to the serial port since connecting it to your computer. In this case there is a buffer of data that we need to read before proceeding. This is the default action of the program so you do not need to specify anything other than your port settings.

The second is that you have either already run this tool since connecting the computer to the printer or used another tool to read the serial port. In this case you need to add the `-skip-ok` option to your command line, otherwise it will hang indefinitely.

## Performance

In testing this code my printer wasn't very predictable in its response times. The `G80` seemed pretty predictable, but the `G1` and `G81` commands sometimes hung for a minute or more.

I do have a built-in timeout of 10 minutes which I think is more than enough to assume that the printer is just MIA.

My point being, if it seems stuck, just give it a few minutes.

## Compatibility

I have tested this on Ubuntu 18.04 with Go 1.12.

There is nothing special here and the serial package I'm using claims cross platform support so it _should_ work for any platform that Go compiles for. I haven't tested it though.

My printer is currently a MK3S with the 3.7.0 firmware.

## Pre-reqs

- An Original Prusa MK3 (2.5 too?) printer
- Access to your serial port
  - On Linux you need to be in the `dialout` group or have sudo access.
- Go 1.11 or 1.12
  - Older may work, but you'll have to fetch the package dependencies manually

## Compiling

`go build -o g81_level_guide main.go comm.go values.go`

## Execution

Fresh connection: `./g81_level_guide -com-port /dev/ttyACM0`

Subsequent: `./g81_level_guide -com-port /dev/ttyACM0 -skip-ok`

Note: Your COM port may vary.

### CLI Options

- `-com-port`: The port to talk to the printer over. On Linux/Mac this will be a TTY device in your `/dev` directory. On Windows it should be the name (e.g. `COM1`). This option is required.
- `-port-speed`: The baud rate of the connection. Defaults to `115200`.
- `-skip-ok`: Don't look for the initial OK if reconnecting to an established session. Default: `false`

### Example Session:

```
$ sudo ./g81_level_guide -com-port /dev/ttyACM0 -skip-ok
2019/04/28 12:10:40 Starting bed calibration
2019/04/28 12:11:38 Fetching results
2019/04/28 12:11:38 Loosen right front screw by 0.45250mm
Again? (Y/n): 
2019/04/28 12:12:00 Starting bed calibration
2019/04/28 12:12:25 Fetching results
2019/04/28 12:12:25 Loosen right rear screw by 0.24167mm
Again? (Y/n): 
2019/04/28 12:13:12 Starting bed calibration
2019/04/28 12:14:04 Fetching results
2019/04/28 12:14:04 Loosen center rear screw by 0.18583mm
Again? (Y/n): 
2019/04/28 12:14:21 Starting bed calibration
2019/04/28 12:15:11 Fetching results
2019/04/28 12:15:11 Loosen left middle screw by 0.16584mm
Again? (Y/n): 
2019/04/28 12:15:31 Starting bed calibration
2019/04/28 12:16:23 Fetching results
2019/04/28 12:16:23 Loosen center front screw by 0.16167mm
Again? (Y/n): 
2019/04/28 12:16:59 Starting bed calibration
2019/04/28 12:17:51 Fetching results
2019/04/28 12:17:51 Loosen right rear screw by 0.15917mm
Again? (Y/n): 
2019/04/28 12:18:11 Starting bed calibration
2019/04/28 12:19:03 Fetching results
2019/04/28 12:19:03 Loosen left rear screw by 0.11084mm
Again? (Y/n): 
2019/04/28 12:19:23 Starting bed calibration
2019/04/28 12:20:14 Fetching results
2019/04/28 12:20:15 Loosen left front screw by 0.11584mm
Again? (Y/n): 
2019/04/28 12:20:29 Starting bed calibration
2019/04/28 12:21:20 Fetching results
2019/04/28 12:21:20 Loosen right rear screw by 0.39000mm
Again? (Y/n): 
2019/04/28 12:21:58 Starting bed calibration
2019/04/28 12:22:50 Fetching results
2019/04/28 12:22:50 Loosen right middle screw by 0.21166mm
Again? (Y/n): 
2019/04/28 12:23:09 Starting bed calibration
2019/04/28 12:24:00 Fetching results
2019/04/28 12:24:00 Loosen center rear screw by 0.16250mm
Again? (Y/n): 
2019/04/28 12:24:16 Starting bed calibration
2019/04/28 12:25:07 Fetching results
2019/04/28 12:25:07 Loosen right rear screw by 0.13500mm
Again? (Y/n): 
2019/04/28 12:25:24 Starting bed calibration
2019/04/28 12:26:15 Fetching results
2019/04/28 12:26:15 Tighten left rear screw by 0.12500mm
Again? (Y/n): 
2019/04/28 12:26:31 Starting bed calibration
2019/04/28 12:27:23 Fetching results
2019/04/28 12:27:23 Tighten left rear screw by 0.22500mm
Again? (Y/n): 
2019/04/28 12:28:16 Starting bed calibration
2019/04/28 12:29:07 Fetching results
2019/04/28 12:29:07 Tighten left rear screw by 0.11583mm
Again? (Y/n): 
2019/04/28 12:29:25 Starting bed calibration
2019/04/28 12:30:17 Fetching results
2019/04/28 12:30:17 Loosen right rear screw by 0.11750mm
Again? (Y/n): 
2019/04/28 12:30:31 Starting bed calibration
2019/04/28 12:31:22 Fetching results
2019/04/28 12:31:22 Loosen right rear screw by 0.10166mm
Again? (Y/n): 
2019/04/28 12:31:40 Starting bed calibration
2019/04/28 12:32:32 Fetching results
2019/04/28 12:32:32 Tighten left middle screw by 0.10500mm
Again? (Y/n): 
2019/04/28 12:32:49 Starting bed calibration
2019/04/28 12:33:41 Fetching results
2019/04/28 12:33:41 Tighten left front screw by 0.08750mm
Again? (Y/n): 
2019/04/28 12:33:57 Starting bed calibration
2019/04/28 12:34:49 Fetching results
2019/04/28 12:34:49 Tighten left rear screw by 0.06583mm
Again? (Y/n): 
2019/04/28 12:35:05 Starting bed calibration
2019/04/28 12:35:57 Fetching results
2019/04/28 12:35:57 Loosen center rear screw by 0.08000mm
Again? (Y/n): 
2019/04/28 12:36:18 Starting bed calibration
2019/04/28 12:37:10 Fetching results
2019/04/28 12:37:10 Tighten left front screw by 0.06083mm
Again? (Y/n): 
2019/04/28 12:37:29 Starting bed calibration
2019/04/28 12:38:20 Fetching results
2019/04/28 12:38:20 Tighten left rear screw by 0.05250mm
Again? (Y/n): 
2019/04/28 12:38:38 Starting bed calibration
2019/04/28 12:39:30 Fetching results
2019/04/28 12:39:30 Loosen right middle screw by 0.04750mm
Again? (Y/n): 
2019/04/28 12:39:44 Starting bed calibration
2019/04/28 12:40:35 Fetching results
2019/04/28 12:40:35 Tighten left rear screw by 0.04167mm
Again? (Y/n): 
2019/04/28 12:40:50 Starting bed calibration
2019/04/28 12:41:41 Fetching results
2019/04/28 12:41:41 Loosen center rear screw by 0.06167mm
Again? (Y/n): 
2019/04/28 12:41:54 Starting bed calibration
2019/04/28 12:42:46 Fetching results
2019/04/28 12:42:46 Tighten left front screw by 0.03500mm
Again? (Y/n): 
2019/04/28 12:43:03 Starting bed calibration
2019/04/28 12:43:54 Fetching results
2019/04/28 12:43:54 Tighten left rear screw by 0.03416mm
Again? (Y/n): 
2019/04/28 12:44:07 Starting bed calibration
2019/04/28 12:44:32 Fetching results
2019/04/28 12:44:32 Loosen right rear screw by 0.03083mm
Again? (Y/n): 
2019/04/28 12:45:13 Starting bed calibration
2019/04/28 12:46:05 Fetching results
2019/04/28 12:46:05 Loosen right middle screw by 0.02250mm
Again? (Y/n): 
2019/04/28 12:46:23 Starting bed calibration
2019/04/28 12:47:14 Fetching results
2019/04/28 12:47:15 Tighten left middle screw by 0.02167mm
Again? (Y/n): 
2019/04/28 12:47:30 Starting bed calibration
2019/04/28 12:48:22 Fetching results
2019/04/28 12:48:22 Tighten left rear screw by 0.01334mm
Again? (Y/n): n
2019/04/28 12:50:39 Final results:
 0.00333	-0.00333	 0.01333
-0.00250	 0.00000	-0.00167
 0.02167	 0.01083	-0.00333
$
```
