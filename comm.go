package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"regexp"

	"github.com/tarm/serial"
)

const bufSize = 1

var eol = regexp.MustCompile(`\sok\s*$`)
var stats = regexp.MustCompile(`Measured points:([\s\.\-0-9]+?)ok`)

func waitForOK(sp *serial.Port, fp io.Writer) (string, error) {
	var err error
	data := new(bytes.Buffer)

	for {
		var size1, size2 int
		buf := make([]byte, bufSize)
		size1, err = sp.Read(buf)
		if err != nil {
			break
		}

		logSerial(fp, buf[0:size1])
		size2, err = data.Write(buf[0:size1])
		if err != nil {
			break
		}
		if size1 != size2 {
			err = fmt.Errorf("unable to collect buffer %d <> %d", size1, size2)
			break
		}

		if eol.MatchString(data.String()) {
			logSerial(fp, []byte("\n"))
			break
		}
	}

	if err != nil && err != io.EOF {
		return "", err
	}

	return data.String(), nil
}

func g80(sp *serial.Port, fp io.Writer) error {
	log.Println("Starting bed calibration")
	g80Cmd := []byte("G80\n")
	logSerial(fp, g80Cmd)
	_, err := sp.Write(g80Cmd)
	if err != nil {
		return err
	}

	if err = sp.Flush(); err != nil {
		return err
	}

	_, err = waitForOK(sp, fp)
	if err != nil {
		return err
	}

	// Get the extruder out of the way so we can work
	// Or as a wonderfully helpful lady says "stick its tongue out" (you rock Joan!)
	g1Cmd := []byte("G1 X10.000 Y200.000 Z100.000 E0\n")
	logSerial(fp, g1Cmd)
	_, err = sp.Write(g1Cmd)
	if err != nil {
		return err
	}

	if err = sp.Flush(); err != nil {
		return err
	}

	_, err = waitForOK(sp, fp)
	if err != nil {
		return err
	}

	return nil
}

func g81(sp *serial.Port, fp io.Writer) (*points, error) {
	log.Println("Fetching results")
	g81Cmd := []byte("G81\n")
	logSerial(fp, g81Cmd)
	_, err := sp.Write(g81Cmd)
	if err != nil {
		return nil, err
	}

	if err = sp.Flush(); err != nil {
		return nil, err
	}

	results, err := waitForOK(sp, fp)
	if err != nil {
		return nil, err
	}

	match := stats.FindAllStringSubmatch(results, -1)
	if len(match) == 0 {
		return nil, errors.New("unable to find stats")
	}

	return processPoints(match[0][1])
}

func logSerial(fp io.Writer, data []byte) {
	if fp == nil {
		return
	}

	fp.Write(data)
}
